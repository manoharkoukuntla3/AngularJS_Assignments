	// create the module and name it scotchApp
	var app = angular.module('main', ['ngRoute']);

	// configure our routes
	app.config(function ($routeProvider) {
	    $routeProvider

	        // route for the home page
	        .when('/', {
	            templateUrl: 'home.html',
	            controller: 'mainController'
	        })

	        // route for the about page
	        .when('/allcds', {
	            templateUrl: 'allcds.html',
	            controller: 'allController'
	        })

	        // route for the contact page
	        .when('/newcds', {
	            templateUrl: 'newcds.html',
	            controller: 'newController'
	        });
	});

	// create the controller and inject Angular's $scope
	app.controller('mainController', function ($scope) {
	    // create a message to display in our view
	    $scope.name = 'CD SHOP';
	    $scope.headOffice = 'SOMEWHERE IN WORLD';

	    $scope.getDetails = function () {
	        return [
	            {
	                title: 'TITLE 1',
	                language: 'ENGLISH',
	                price: 10
            },
	            {
	                title: 'TITLE 2',
	                language: 'SPANISH',
	                price: 6
            },
	            {
	                title: 'TITLE 3',
	                language: 'ENGLISH',
	                price: 12
            },
	            {
	                title: 'TITLE 4',
	                language: 'ENGLISH',
	                price: 8
            },
	            {
	                title: 'TITLE 5',
	                language: 'FRENCH',
	                price: 9
            }
                ]
	    }
	});

	app.controller('allController', function ($scope) {
	    $scope.name = 'CD SHOP';
	    $scope.headOffice = 'SOMEWHERE IN WORLD';

	    $scope.getDetails = function () {
	        return [
	            {
	                title: 'TITLE 1',
	                language: 'ENGLISH',
	                price: 10
            },
	            {
	                title: 'TITLE 2',
	                language: 'SPANISH',
	                price: 6
            },
	            {
	                title: 'TITLE 3',
	                language: 'ENGLISH',
	                price: 12
            },
	            {
	                title: 'TITLE 4',
	                language: 'ENGLISH',
	                price: 8
            },
	            {
	                title: 'TITLE 5',
	                language: 'FRENCH',
	                price: 9
            },
	            {
	                title: 'TITLE 6',
	                language: 'ITALIAN',
	                price: 10
            },
	            {
	                title: 'TITLE 7',
	                language: 'HINDI',
	                price: 16
            },
	            {
	                title: 'TITLE 8',
	                language: 'FRENCH',
	                price: 18
            },
	            {
	                title: 'TITLE 9',
	                language: 'RUSSIAN',
	                price: 15
            }

                ]
	    }
	});

	app.controller('newController', function ($scope) {
	    $scope.name = 'CD SHOP';
	    $scope.headOffice = 'SOMEWHERE IN WORLD';

	    $scope.getDetails = function () {
	        return [
	            {
	                title: 'TITLE 1',
	                language: 'ENGLISH',
	                price: 10
            },
	            {
	                title: 'TITLE 2',
	                language: 'SPANISH',
	                price: 6
            },
	            {
	                title: 'TITLE 5',
	                language: 'FRENCH',
	                price: 9
            },
	            ]
	    }
	});

var cont = angular.module('main', []);
cont.controller('CDController', function ($scope) {
    $scope.name = 'CD SHOP';
    $scope.headOffice = 'SOMEWHERE IN WORLD';

    $scope.getDetails = function () {
        return [
            {
                title: 'TITLE 1',
                language: 'ENGLISH',
                price: 10
            },
            {
                title: 'TITLE 2',
                language: 'SPANISH',
                price: 6
            },
            {
                title: 'TITLE 3',
                language: 'ENGLISH',
                price: 12
            },
            {
                title: 'TITLE 4',
                language: 'ENGLISH',
                price: 8
            },
            {
                title: 'TITLE 5',
                language: 'FRENCH',
                price: 9
            },
            {
                title: 'TITLE 6',
                language: 'ITALIAN',
                price: 10
            }
                ]
    }
});
